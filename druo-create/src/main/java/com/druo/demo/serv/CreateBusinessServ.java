package com.druo.demo.serv;

import java.util.List;

import com.druo.demo.db.entity.Business;
import com.druo.demo.dto.CreateReqDTO;

public interface CreateBusinessServ {

	public Business create(CreateReqDTO request);
	
	public List<Business> list();
}
