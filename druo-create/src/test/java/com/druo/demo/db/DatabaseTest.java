package com.druo.demo.db;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.druo.demo.DemoApplicationTests;
import com.druo.demo.db.entity.Business;
import com.druo.demo.db.repository.BusinessRepository;

public class DatabaseTest extends DemoApplicationTests {
	
	@Autowired
	private BusinessRepository br;
	
	@Test
    public void okInsert() throws Exception {
		Business business = new Business();
		business.setId(br.count() + 1);
		business.setNit("123456-test");
		business.setName("test");
		business.setMail("test@test");
		br.save(business );
		
		// validate if exist
		assertNotNull(br.findByNit("123456-test"));
		delete("123456-test");
    }
	
	@Test(expected = Exception.class)
    public void badNitInsert() throws Exception {
		Business business = new Business();
		business.setId(br.count() + 1);
		//business.setNit("123456-test");
		business.setName("test");
		business.setMail("test@test");
		br.save(business );
    }
	
	@Test(expected = Exception.class)
    public void badIdInsert() throws Exception {
		Business business = new Business();
		//business.setId(br.count() + 1);
		business.setNit("123456-test");
		business.setName("test");
		business.setMail("test@test");
		br.save(business );
    }
	
	@Test(expected = Exception.class)
    public void badNameInsert() throws Exception {
		Business business = new Business();
		business.setId(br.count() + 1);
		business.setNit("123456-test");
		//business.setName("test");
		business.setMail("test@test");
		br.save(business );
    }
	
	@Test(expected = Exception.class)
    public void badMailInsert() throws Exception {
		Business business = new Business();
		business.setId(br.count() + 1);
		business.setNit("123456-test");
		business.setName("test");
		//business.setMail("test@test");
		br.save(business );
    }

	private void delete(String Nit) {
		Business business = br.findByNit(Nit);
		if (business != null) {
			br.delete(business);
		}
	}
}
