package com.druo.demo.controller;

import static org.mockito.Matchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.druo.demo.DemoApplicationTests;
import com.druo.demo.db.entity.Business;
import com.druo.demo.dto.CreateReqDTO;
import com.druo.demo.serv.CreateBusinessServ;

@AutoConfigureMockMvc
public class OperationsTest extends DemoApplicationTests {
	
	@Autowired
    private MockMvc mockMvc;

    @MockBean
    private CreateBusinessServ bsMock;
    
    @Before
    public void setup() throws Exception {
    	Business b = new Business();
        given(bsMock.list()).willReturn(new LinkedList<>());
        given(bsMock.create(any(CreateReqDTO.class))).willReturn(b);

    }
    
    @Test
    public void badCreateRequest() throws Exception {
        mockMvc.perform(post("/business/create")
                .contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void okCreateRequest() throws Exception {
        mockMvc.perform(post("/business/create")
                .contentType(MediaType.APPLICATION_JSON).content("{\"nit\": \"1236\", \"name\": \"test2\", \"mail\": \"asasd\"}"))
                .andExpect(status().is2xxSuccessful());
    }
}
