package com.druo.demo.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.druo.demo.db.entity.Business;

@Repository
public interface BusinessRepository extends JpaRepository<Business, String>{

	public Business findByNit(String nit);
}
