package com.druo.demo.db.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Business implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8336543438862932538L;

	@NotNull
	@Column(
            nullable = false)
	private String name;
	
	@NotNull
	@Column(
            nullable = false)
	private String nit;
	
	@NotNull
	@Column(
            nullable = false)
	private String mail;
	
	@Id
	private Long id;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNit() {
		return nit;
	}
	public void setNit(String nit) {
		this.nit = nit;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
}
