package com.druo.demo.domain;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice(
        annotations = RestController.class)
public class ResponseHandler {

	//@ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<Object> maskError(
            final HttpServletRequest pRequest, final HttpServletResponse pResponse,
            final Exception ex) {
        
        return ResponseEntity.badRequest().header("error", "Something happen, try later.").body(null);
    }
}
