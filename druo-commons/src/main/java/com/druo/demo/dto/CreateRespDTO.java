package com.druo.demo.dto;

import javax.validation.constraints.NotEmpty;

public class CreateRespDTO {

	@NotEmpty
	private String name;

	@NotEmpty
	private String nit;
	
	@NotEmpty
	private String mail;
	
	private String id;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNit() {
		return nit;
	}
	public void setNit(String nit) {
		this.nit = nit;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
