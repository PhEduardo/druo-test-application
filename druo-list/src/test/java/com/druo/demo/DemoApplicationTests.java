package com.druo.demo;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.druo.demo.DemoApplication;

@ComponentScan({
    "com.druo.demo"
})
@RunWith(SpringRunner.class)
@SpringBootTest(
        properties = {
            "server.context-path=/",
        },
        classes = DemoApplication.class)
@DirtiesContext
@TestPropertySource("classpath:test.properties")
public abstract class DemoApplicationTests {

}

