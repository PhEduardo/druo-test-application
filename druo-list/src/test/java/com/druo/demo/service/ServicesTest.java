package com.druo.demo.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.druo.demo.DemoApplicationTests;
import com.druo.demo.db.entity.Business;
import com.druo.demo.db.repository.BusinessRepository;
import com.druo.demo.dto.CreateReqDTO;
import com.druo.demo.serv.CreateBusinessServ;

public class ServicesTest extends DemoApplicationTests {

	@Autowired
    private CreateBusinessServ businnesServ;
	
	@Autowired
	private BusinessRepository br;
	
	@Test
    public void okListService() throws Exception {
		long preSize = br.count();
		CreateReqDTO requestOk = new CreateReqDTO();
		requestOk.setNit("123456-test");
		requestOk.setName("test");
		requestOk.setMail("test@test");
		businnesServ.create(requestOk);
		long postSize = br.count();
		assertEquals(preSize + 1, postSize);
		
		delete("123456-test");
    }
	
	private void delete(String Nit) {
		Business business = br.findByNit(Nit);
		if (business != null) {
			br.delete(business);
		}
	}
}
