package com.druo.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.druo.demo.db.entity.Business;
import com.druo.demo.serv.AuthorizationServ;

@RestController
@RequestMapping("authorization")
public class AuthorizationController {
	
	@Autowired
	private AuthorizationServ auth;
	
	@RequestMapping(
			value = "token",
            method = RequestMethod.GET
			)
	public ResponseEntity<String> answerList(HttpServletRequest request) {
		String token = auth.getToken("test");
		return ResponseEntity.ok().body(token);
	}
}
