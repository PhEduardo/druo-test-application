package com.druo.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.druo.demo.db.entity.Business;
import com.druo.demo.dto.CreateReqDTO;
import com.druo.demo.dto.CreateRespDTO;
import com.druo.demo.serv.CreateBusinessServ;

@RestController
@RequestMapping("business")
public class OperationsController {
	
	@Autowired
	private CreateBusinessServ createBusiness;
	 
	@RequestMapping(
			value = "create",
            method = RequestMethod.POST
			)
	public ResponseEntity<Business> answer(HttpServletRequest request, @RequestBody @Valid final CreateReqDTO createRequest) {
		Business response = createBusiness.create(createRequest);
		
		if (response != null)
			return ResponseEntity.ok().body(response);
		else 
			return ResponseEntity.badRequest().header("messsage", "Business Already Exists").body(null);
	}

	@RequestMapping(
			value = "list",
            method = RequestMethod.GET
			)
	public ResponseEntity<List<Business>> answerList(HttpServletRequest request) {
		List<Business> response = createBusiness.list();
		return ResponseEntity.ok().body(response);
	}
}
