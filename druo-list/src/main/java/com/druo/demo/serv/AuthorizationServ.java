package com.druo.demo.serv;

public interface AuthorizationServ {
	public String getToken(String username);
}
