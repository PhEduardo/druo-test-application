package com.druo.demo.serv.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.druo.demo.db.entity.Business;
import com.druo.demo.db.repository.BusinessRepository;
import com.druo.demo.dto.CreateReqDTO;
import com.druo.demo.serv.CreateBusinessServ;

@Service
public class CreateBusinessServImpl implements CreateBusinessServ {
	
	@Autowired
	private BusinessRepository br;
	
	@Override
	public Business create(CreateReqDTO request) {
		// validate if business exist
		if (br.findByNit(request.getNit()) != null) {
			return null;
		}
		
		else {
			Business cloneReq = new Business();
			cloneReq.setId(br.count() + 1);
	        BeanUtils.copyProperties(request, cloneReq, "id");
	        br.save(cloneReq);
			return cloneReq;
		}
	}
	
	@Override
	public List<Business> list(){
		return br.findAll();
	}
}
